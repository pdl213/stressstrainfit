#!/usr/bin/python

import math
import os
import sys
from numpy import *
#import numpy as np
import pylab as pl
from SimplifyLine import * 
from Regress0Intercept import *


if __name__ == "__main__":
    #User inputs for finding new line
    tol = .00010 
    NewPoints = 9 #This is the max for Dyna3D material 24 (8 points)
    BreakStrain = .02 #Make straight from here out

    #Test Data File
    TestData = '../IMP69-A12-Sts-stn.txt'
    
    #ploting and output parameters
    TextOutput = 'TabulatedPoints.txt'
    OutputFile = ['Wrong','ReducedLine.eps','Reductions.eps']
    Title = ['Wrong','Fit','Individual Fit','Fitting Progress']
    color = ['r', 'g', 'b', 'm', 'c','k', 'y', '0.5','0.25']
    TestName=['NonTest','Total','Seg 1','Seg 2','Seg 3','Seg 4','Seg 5','Seg 7','Seg 8']
    plot1=[] #Final Answer Plot
    plot2=[] #Progress Plot
    io = open(TextOutput,'w')

    #Intialized some variables
    length = NewPoints+2
    CurTol = tol/2.0
    x=[]
    y=[]
    count=0
    j=0

    #Read from input file and plot
    stress,strain,truestress,trueplasticstrain = loadtxt(TestData,comments='#',unpack=True)
    pl.figure(1)
    plot1.append(pl.plot(trueplasticstrain,truestress,color[0],label='Data'))
    pl.ylabel('Stress')
    pl.xlabel('Strain')
    pl.figure(2)
    plot2.append(pl.plot(trueplasticstrain,truestress,color[0],label='Data'))
    pl.ylabel('Stress')
    pl.xlabel('Strain')
    pl.draw()

    #Determine limits on data
    minstrain=min(trueplasticstrain)
    maxstrain=max(trueplasticstrain)
    minstress=min(truestress)
    maxstress=max(truestress)

    #Remove linear part from points
    fulllen = len(trueplasticstrain)
    for j in range(0,fulllen):
        if(trueplasticstrain[j]>0.0):
            LinearLimit = j
            TempStress = truestress[LinearLimit:fulllen]
            TempStrain = trueplasticstrain[LinearLimit:fulllen]
            truestress=TempStress
            trueplasticstrain=TempStrain
            break

    #Remove back part of data for straight section
    if(maxstrain>BreakStrain):
        StraightSeg=True
        InitLength=len(trueplasticstrain)
        print('Using a straight line segment for a portion of the line')
        for i in range(0,InitLength):
            if(trueplasticstrain[i]>=BreakStrain):
                FinalPoints=i
                StX=trueplasticstrain[FinalPoints:InitLength]
                StY=truestress[FinalPoints:InitLength]
                StPoints=LineSlope(StX,StY)
                break
        FitStrain=trueplasticstrain[0:FinalPoints+1]
        FitStress=truestress[0:FinalPoints+1]
    else:
        FitStrain=trueplasticstrain
        FitStress=truestress
        StraightSeg=False

    while(length>NewPoints): 
        count=count+1
        if(count>1):
            x[:]=[]
            y[:]=[]
        j=j+1
        if(j>=len(color)):
            j=j-len(color)
        CurTol = CurTol+tol

        keep= simplify_points(FitStrain,FitStress,CurTol)
        length=len(keep)
        for i in range(0,length):
            x.append(trueplasticstrain[keep[i]])
            y.append(truestress[keep[i]])
        if(StraightSeg):
            x.append(StPoints[0][1])
            y.append(StPoints[1][1])
            length=length+1
        labelname='Pass #'+str(count)
        print('There are %i points when the tolerance is %f' %(length,CurTol))
        plot2.append(pl.plot(x,y,color[j],label=labelname))


    pl.figure(1)
    plot1.append(pl.plot(x,y,'g-*',label='Reduced Line'))        
    print('******SUMMARY********')
    print('There are %i points when the tolerance is %f' %(length,CurTol))
    io.write("There are %i points when the tolerance is %f\n" %(length,CurTol))
    io.write("Values of yield stress and plastic strain at initial yeild\n")
    io.write("%10.2e%10.2e\n" %(x[0],y[0]))
    for i in range(0,length):
        print('%f, %f' %(x[i],y[i]))
    for i in range(1,length):
        io.write("%10.2e" %(x[i]))
    io.write("\n")
    for j in range(1,length):
        io.write("%10.2e" %(y[j]))
    io.write("\n")
    pl.savefig(OutputFile[1],format='eps',transparent=True)
    pl.figure(2)
    pl.savefig(OutputFile[2],format='eps',transparent=True)
   





