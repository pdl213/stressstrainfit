import os
import sys
import math
from scipy import stats 
from numpy import *
#import numpy as np
import pylab as pl

def LineSlope(x,y):
    
    xpoints=[]
    ypoints=[]

    minx= min(x)
    maxx= max(x)
    miny= min(y)
    
    x=x-minx
    y=y-miny
    
    x=x[:,newaxis]
    
    a, _, _, _ = linalg.lstsq(x,y )
    
    xpoints.append(minx)
    ypoints.append(a*(minx-minx)+miny)
    xpoints.append(maxx)
    ypoints.append(a*(maxx-minx)+miny)
    
    return(xpoints,ypoints)
